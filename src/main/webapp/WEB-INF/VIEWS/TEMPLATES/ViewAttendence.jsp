<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ViewAttendence</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body ng-app="UserApp">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="EmployeeHeader.jsp"></jsp:include>
			</div>
		</div>

		<div class="row" ng-controller="ViewAttendenceController">
			<div class="col-sm-12" style="background-color: lavenderblush;">
				<div class="card-group">
					<div class="card text-white bg-info ">
						<h4 class="card-header">View Your Attendence</h4>
						<div class="card-body">
							<form class="form-inline" ng-submit="findAttendence()">
								<div class="input-group mb-2 mr-sm-2">
									<div class="input-group-prepend">
										<div class="input-group-text">From</div>
									</div>
									<input type="text" class="form-control form-control-sm"
										id="inlineFormInputGroupUsername1" datepicker ng-model="emp.from" >
								</div>
								<div class="input-group mb-2 mr-sm-2">
									<div class="input-group-prepend">
										<div class="input-group-text">To</div>
									</div>
									<input type="text" class="form-control form-control-sm"
										id="inlineFormInputGroupUsername2" datepicker ng-model="emp.to" >
								</div>

								<div class="form-check mb-2 mr-sm-2">
									<button type="submit" class="btn btn-primary btn-sm mb-2">Submit</button>
								</div>


							</form>
							<table class="table table-sm-responsive">
								<thead>
									<tr>
										<th>Sr.No.</th>
										<th>TaskName</th>
										<th>TaskDescription</th>
										<th>AssignedDate</th>
										
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="FA in filterAttendences">

										<td>{{$index+1}}</td>
										<td>{{FA.attendence_date}}</td>
										<td>{{FA.attendence_time}}</td>
										<td>{{FA.presence}}</td>
										
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<link
		href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css"
		rel="stylesheet" type="text/css" />
	<script
		src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
	<script
		src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/UserModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/UserCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/UserService.js"></c:url>'></script>

</body>
</html>