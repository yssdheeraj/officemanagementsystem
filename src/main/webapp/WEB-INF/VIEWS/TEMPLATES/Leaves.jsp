<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Leave</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body ng-app="UserApp">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="EmployeeHeader.jsp"></jsp:include>
			</div>
		</div>

		<div class="row" ng-controller="LeaveController">
			<div class="col-sm-12" style="background-color: lavenderblush;">
				<div class="card-group">
					<div class="card text-white bg-info ">
						<h4 class="card-header">Take Leave</h4>
						<div class="card-body">
							<form class="form-control-horizontal" ng-submit="postLeave()">
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">From</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											datepicker ng-model="employee.start_date" />
									</div>
								</div>
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">To</label>
									<div class="col-sm-6">
										<input type="text" class="form-control form-control-sm"
											datepicker ng-model="employee.end_date" />
									</div>
								</div>
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Reason</label>
									<div class="col-sm-6">
										<textarea class="form-control form-control-sm"
											ng-model="employee.reason" name="reason"></textarea>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-10 offset-sm-2">
										<button type="submit" class="btn btn-warning btn-sm">Ok</button>
										<!-- <button type="reset" class="btn btn-danger btn-sm">Reset</button> -->
										<a type="button" class="btn btn-danger btn-sm"
											ng-click="_reset()">Clear</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="card text-white bg-info">
						<h4 class="card-header">Leaves</h4>
						<div class="card-body">
							<table class="table table-sm-responsive">
								<thead>
									<tr>
										<th>Sr.No.</th>
										<th>StartDate</th>
										<th>EndDate</th>
										<th>Reason</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="L in leaveList">

										<td>{{$index+1}}</td>
										<td>{{L.start_date}}</td>
										<td>{{L.end_date}}</td>
										<td>{{L.reason}}</td>
										<td><a ng-click="editLeave(L)"
											class="btn btn-warning btn-sm" role="button">Edit</a> | <a
											ng-click="removeLeave(L)" class="btn btn-danger btn-sm"
											role="button">Delete</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
	<!-- <link href="/js/lib/jquery-ui.min.css" rel="stylesheet">
	<script src="/js/lib/jquery-ui.min.js"></script> --> 
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" /> <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script> 
	
	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/UserModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/UserCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/UserService.js"></c:url>'></script>
	
</body>
</html>