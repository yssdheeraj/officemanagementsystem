<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>ViewTask</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body ng-app="UserApp">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="EmployeeHeader.jsp"></jsp:include>
			</div>
		</div>

		<div class="row" ng-controller="ViewTaskController">
			<div class="col-sm-12" style="background-color: lavenderblush;">
				<div class="card-group">
					<div class="card text-white bg-info ">
						<h4 class="card-header">View Your Task</h4>
						<div class="card-body">
							<table class="table table-sm-responsive">
								<thead>
									<tr>
										<th>Sr.No.</th>
										<th>TaskName</th>
										<th>TaskDescription</th>
										<th>AssignedDate</th>
										<th>CompletionDate</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="T in tasks">

										<td>{{$index+1}}</td>
										<td>{{T.task_name}}</td>
										<td>{{T.task_description}}</td>
										<td>{{T.assigned_date}}</td>
										<td>{{T.completion_date}}</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/UserModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/UserCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/UserService.js"></c:url>'></script>

</body>
</html>