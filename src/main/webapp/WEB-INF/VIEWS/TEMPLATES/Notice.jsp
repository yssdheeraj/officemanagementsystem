<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Notice</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body ng-app="AdminApp">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="AdminHeader.jsp"></jsp:include>
			</div>
		</div>
		<div class="row" ng-controller="NoticeController">
			<div class="col-sm-12">
				<div class="card text-white bg-info">
					<h4 class="card-header">BroadCast Notice</h4>
					<div class="card-body">
						<form class="form-control-horizontal" ng-submit="postNotice()">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Notice</label>
								<div class="col-sm-6">
									<textarea class="form-control form-control-sm"
										ng-model="admin.notice" name="notice"></textarea>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10 offset-sm-2">
									<button type="submit" class="btn btn-warning btn-sm">Submit</button>
								</div>
							</div>
						</form>

						<!-- 						<table class="table table-sm-responsive">
							<thead>
								<tr>
									<th>Sr.No.</th>
									<th>UserName</th>
									<th>Status</th>
									<th>Present</th>
									<th>Absent</th>
									<th>Update</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="TA in todayAttendences">

									<td>{{$index+1}}</td>
									<td>{{TA.username}}</td>
									<td>{{TA.presence}}</td>
									<td><input type="checkbox" ng-model="TA.presence"
										ng-true-value="'Present'" /></td>
									<td><input type="checkbox" ng-model="TA.presence"
										ng-true-value="'Absent'" /></td>
									<td><a ng-click="updateAttendence(TA)"
										class="btn btn-warning btn-sm" role="button">Update</a></td>
								</tr>
							</tbody>
						</table> -->
					</div>
				</div>
			</div>
		</div>
	</div>

	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/AdminModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/AdminCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/AdminService.js"></c:url>'></script>

</body>
</html>