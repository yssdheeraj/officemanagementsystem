<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>
<body ng-app="UserApp" ng-controller="UserController">

	<div class="card text-white bg-warning">
		<h4 class="card-header">UserProfile</h4>
		<div class="card-body">
			<form class="form-control-horizontal" ng-submit="submitUser()">

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">first_name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="first_name" name="first_name"
							ng-model="user.first_name">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">middle_name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="middle_name" name="middle_name"
							ng-model="user.middle_name">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">last_name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="last_name" name="last_name"
							ng-model="user.last_name">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">joined_date</label>
					<div class="col-sm-6">
						<!-- <input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="joined_date" name="joined_date"
							ng-model="user.joined_date"> -->
						<input type="text" class="form-control form-control-sm" datepicker ng-model="user.joined_date" />
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">UserName</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="username" name="username"
							ng-model="user.username">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">email</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="Email" name="email"
							ng-model="user.email">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">password</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="password" name="password"
							ng-model="user.password">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">gender</label>
					<div class="col-sm-6">
						<select ng-model="user.gender"
							class="form-control form-control-sm">
							<option ng-repeat="G in genders" value="{{G.genderSelect}}">{{G.genderSelect}}</option>
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Role</label>
					<div class="col-sm-6">
						<select ng-model="user.user_role"
							class="form-control form-control-sm">
							<option ng-repeat="R in roles" value="{{R.rolename}}">{{R.rolename}}</option>
						</select>
					</div>
				</div>


				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">designation</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="designation" name="designation"
							ng-model="user.designation">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">address</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="address" name="address"
							ng-model="user.address">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">contact</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="contact" name="contact"
							ng-model="user.contact">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Upload
						Photo</label>
					<div class="col-sm-6">
						<!--   <input type="file" class="form-control form-control-sm" ng-file-select="onFileSelect($files)" ng-model="imageSrc"> -->
						<!-- <input type="file" class="form-control form-control-sm"
							ng-model="user.profile_picture"> -->
						<input type="file" name="profile_picture"
							onchange="angular.element(this).scope().uploadedFile(this);"
							class="form-control form-control-sm">
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10 offset-sm-2">
						<button type="submit" class="btn btn-info btn-sm">Submit</button>
						<button type="reset" class="btn btn-danger btn-sm">Reset</button>
						<a type="button" class="btn btn-danger btn-sm" href="/ahome">Back</a>
					</div>
				</div>
			</form>
		</div>
	</div>

<!-- <script src="/webjars/jquery/3.2.1/jquery.min.js"></script> -->
	<!-- <link href="/js/lib/jquery-ui.min.css" rel="stylesheet">
	<script src="/js/lib/jquery-ui.min.js"></script> --> 
	<link href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css" rel="stylesheet" type="text/css" /> <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script> 
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/UserModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/UserCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/UserService.js"></c:url>'></script>
</html>