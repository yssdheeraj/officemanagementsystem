<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>EmployeeDashboard</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>
<body ng-app="EmployeeApp" ng-controller="EmployeeHomeController">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<nav
					class="navbar navbar-expand-sm bg-primary navbar-dark sticky-top">
				<!-- Brand/logo --> <!-- <a class="navbar-brand" href="#"> <img
					src="office _logo.jpg" alt="logo" style="width: 40px;"> --> </a> <!-- Links -->
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link" href="#/">ViewAttendence</a></li>
					<li class="nav-item"><a class="nav-link" href="#/attendence">ViewTasks</a></li>
					<li class="nav-item"><a class="nav-link" href="#/tasks">ViewNotice</a></li>
					<li class="nav-item"><a class="nav-link" href="#/takeLeave">TakeLeave</a></li>
					<li class="nav-item"><a class="nav-link" href="#/users">ReportComplaints</a></li>
					<li class="nav-item"><a class="nav-link" href="#/complaints">ChangeProfileDetails</a></li>
					<li class="nav-item"><a class="nav-link" href="#/complaints">Logout</a></li>
				</ul>
				</nav>
				<!-- <div class="col-sm-2" style="background-color: orange;">OMS</div>
			<div class="col-sm-8" style="background-color: yellow;">
				Attendence,Tasks,View Users,Complaints</div>


			<div class="col-sm-2" style="background-color: skyblue;">Data/Time</div> -->
			</div>
		</div>



		<div class="row">
			<div class="col-sm-4" style="background-color: lavender;">
				<div class="alert alert-warning alert-dismissible fade show"
					role="alert">
					<strong>ProfilePicture <br> Name: <br>Address: <br>Contact: <br>Gender: </strong>
				</div>

			</div>
			<div class="col-sm-8" style="background-color: lavenderblush;">
				<button ng-click="showemp()">show</button>
			</div>
		</div>
	</div>
	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/EmployeeModel.js"></c:url>'></script>
	<script
		src='<c:url value="/js/controller/EmployeeController.js"></c:url>'></script>
	<script
		src='<c:url value="/js/service/EmployeeService.js"></c:url>'></script>

</body>
</html>