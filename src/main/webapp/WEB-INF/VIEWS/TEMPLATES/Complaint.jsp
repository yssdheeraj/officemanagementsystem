<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Complaint</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<!-- <link rel="stylesheet" href="https://rawgit.com/esvit/ng-table/master/dist/ng-table.min.css"> -->

<!-- <script src="https://rawgit.com/esvit/ng-table/master/dist/ng-table.min.js"></script> -->


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body ng-app="UserApp">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="EmployeeHeader.jsp"></jsp:include>
			</div>
		</div>



		<div class="row" ng-controller="ComplaintController">
			<div class="col-sm-12" style="background-color: lavenderblush;">
				<div class="card-group">
					<div class="card text-white bg-warning ">
						<h4 class="card-header">Add Complaint</h4>
						<div class="card-body">
							<form class="form-control-horizontal" ng-submit="postComplaint()">
								<div class="form-group row">
									<label for="inputEmail3" class="col-sm-2 col-form-label">Complaint</label>
									<div class="col-sm-6">
										<textarea class="form-control form-control-sm"
											ng-model="employee.complaint" name="complaint"></textarea>
									</div>
								</div>
								<div class="form-group row">
									<div class="col-sm-10 offset-sm-2">
										<button type="submit" class="btn btn-info btn-sm">Submit</button>
										<!-- <button type="reset" class="btn btn-danger btn-sm">Reset</button> -->
										<a type="button" class="btn btn-danger btn-sm" ng-click="reset()">Clear</a>
									</div>
								</div>
							</form>
						</div>
					</div>
					<div class="card text-white bg-info">
						<h4 class="card-header">Complaint List</h4>
						<div class="card-body">
							<table class="table table-sm-responsive">
								<thead>
									<tr>
										<th>Sr.No.</th>
										<th>Complaint</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									<tr ng-repeat="c in complaintsList">

										<td>{{$index+1}}</td>
										<td>{{c.complaint}}</td>
										<td><a ng-click="editComplaint(c)"
											class="btn btn-warning btn-sm" role="button">Edit</a> | <a
											ng-click="removeComplaint(c)" class="btn btn-danger btn-sm"
											role="button">Delete</a></td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/UserModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/UserCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/UserService.js"></c:url>'></script>
</body>
</html>