<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Report</title>

<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<style type="text/css">
.col-form-label {
	padding-top: calc(-0.625rem + 1px);
	padding-bottom: calc(.375rem + 1px);
	margin-bottom: 0;
	font-size: inherit;
	line-height: 0.5;
	color: darkblue;
}

body {
	margin: 0;
	font-family: -apple-system, BlinkMacSystemFont, "Segoe UI", Roboto,
		"Helvetica Neue", Arial, sans-serif, "Apple Color Emoji",
		"Segoe UI Emoji", "Segoe UI Symbol";
	font-size: 1rem;
	font-weight: 400;
	line-height: 0.5;
	color: #212529;
	text-align: left;
	background-color: #fff;
}
}
</style>

</head>
<body ng-app="AdminApp">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="AdminHeader.jsp"></jsp:include>
			</div>
		</div>
		<div class="row" ng-controller="ViewReportController">
			<div class="col-sm-12">
				<div class="card text-white bg-info">
					<div class="card-body">
						<div class="form-group row">
							<div class="col-sm-2">
								<select ng-model="data.username"
									class="form-control form-control-sm" ng-change="selectAction()">
									<option value="">---Please select---</option>
									<!-- not selected / blank option -->
									<option ng-repeat="R in viewemployees" value="{{R.username}}">{{R.first_name}}
										{{R.middle_name}} {{R.last_name}}</option>
								</select>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card bg-warning">
					<div class="card-body">
						<div class="form-group row">
							<label for="first_name" class="col-xs-3 col-form-label mr-2">First
								Name:</label>
							<div class="col-xs-9">{{employeeReport.first_name}}
								{{employeeReport.middle_name}} {{employeeReport.last_name}}</div>
						</div>
						<div class="form-group row">
							<label for="last_name" class="col-xs-3 col-form-label mr-2">Address:
							</label>
							<div class="col-xs-9">{{employeeReport.address}}</div>
						</div>
						<div class="form-group row">
							<label for="last_name" class="col-xs-3 col-form-label mr-2">Contact:
							</label>
							<div class="col-xs-9">{{employeeReport.contact}}</div>
						</div>
						<div class="form-group row">
							<label for="last_name" class="col-xs-3 col-form-label mr-2">Designation:
							</label>
							<div class="col-xs-9">{{employeeReport.designation}}</div>
						</div>
						<div class="form-group row">
							<label for="last_name" class="col-xs-3 col-form-label mr-2">Email:
							</label>
							<div class="col-xs-9">{{employeeReport.email}}</div>
						</div>
						<div class="form-group row">
							<label for="last_name" class="col-xs-3 col-form-label mr-2">Gender:
							</label>
							<div class="col-xs-9">{{employeeReport.gender}}</div>
						</div>
						<div class="form-group row">
							<label for="last_name" class="col-xs-3 col-form-label mr-2">Joined
								Date: </label>
							<div class="col-xs-9">{{employeeReport.joined_date}}</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6">
				<div class="card bg-warning">
					<div class="card-body">

						<table class="table table-sm">
							<thead>
								<tr>
									<th>SrNo</th>
									<th>Assigned Date</th>
									<th>Completion Date</th>
									<th>Task Name</th>
									<th>Task Description</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="TL in taskList">
									<td>{{$index+1}}</td>
									<td>{{TL.assigned_date}}</td>
									<td>{{TL.completion_date}}</td>
									<td>{{TL.task_name}}</td>
									<td>{{TL.task_description}}</td>
								</tr>

							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr>
									<th>SrNo</th>
									<th>Start Date</th>
									<th>End Date</th>
									<th>Reason</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="LL in leaveList">
									<td>{{$index+1}}</td>
									<td>{{LL.start_date}}</td>
									<td>{{LL.end_date}}</td>
									<td>{{LL.reason}}</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr>
									<th>SrNo</th>
									<th>Complaint</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="CL in complaintList ">
									<td>{{$index+1}}</td>
									<td>{{CL.complaint}}</td>
								</tr>
							</tbody>
						</table>
						<table class="table table-sm">
							<thead>
								<tr>
									<th>SrNo</th>
									<th>Attendence Date</th>
									<th>Attendence Time</th>
									<th>Attendence</th>
								</tr>
							</thead>
							<tbody>
								<tr ng-repeat="AL in attendenceList">
									<td>{{$index+1}}</td>
									<td>{{AL.attendence_date}}</td>
									<td>{{AL.attendence_time}}</td>
									<td>{{AL.presence}}</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>
		</div>

		<script src="/js/lib/angular.min.js"></script>
		<script src='<c:url value="/js/model/AdminModel.js"></c:url>'></script>
		<script src='<c:url value="/js/controller/AdminCtrl.js"></c:url>'></script>
		<script src='<c:url value="/js/service/AdminService.js"></c:url>'></script>
</body>
</html>