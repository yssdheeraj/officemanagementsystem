<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>User</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
</head>
<body>
<body ng-app="UserApp">
	<div class="container-fluid">
		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="EmployeeHeader.jsp"></jsp:include>
			</div>
		</div>

		<div class="row" ng-controller="UserProfileController">
			<div class="col-sm-12" style="background-color: lavenderblush;">
				<div class="card text-white bg-danger">
					<h4 class="card-header">UserProfile</h4>
					<div class="card-body">
						<form class="form-control-horizontal" ng-submit="updateUserProfile()">
							
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">gender</label>
								<div class="col-sm-6">
									<select ng-model="user.gender"
										class="form-control form-control-sm">
										<option ng-repeat="G in genders" value="{{G.genderSelect}}">{{G.genderSelect}}</option>
									</select>
								</div>
							</div>


							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">address</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										id="inputEmail3" placeholder="address" name="address"
										ng-model="user.address">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">contact</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										id="inputEmail3" placeholder="contact" name="contact"
										ng-model="user.contact">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-2 col-form-label">Upload
									Photo</label>
								<div class="col-sm-6">
									<!--   <input type="file" class="form-control form-control-sm" ng-file-select="onFileSelect($files)" ng-model="imageSrc"> -->
									<!-- <input type="file" class="form-control form-control-sm"
							ng-model="user.profile_picture"> -->
									<input type="file" name="profile_picture"
										onchange="angular.element(this).scope().uploadedFile(this);"
										class="form-control form-control-sm">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10 offset-sm-2">
									<button type="submit" class="btn btn-primary btn-sm">Update</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>



	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/UserModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/UserCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/UserService.js"></c:url>'></script>
</html>