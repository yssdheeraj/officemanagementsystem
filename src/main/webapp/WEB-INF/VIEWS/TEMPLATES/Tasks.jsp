<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Tasks</title>
<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


</head>
<body ng-app="AdminApp">
	<div class="container-fluid">

		<div class="row">
			<div class="col-sm-12">
				<jsp:include page="AdminHeader.jsp"></jsp:include>
			</div>
		</div>
		<div class="row" ng-controller="TaskController">
			<div class="col-sm-12">
				<div class="card text-white bg-info">
					<div class="card-header">
						<p class="text-xl-center">
							Task &nbsp;&nbsp;&nbsp; <input type="radio" ng-model="myVar"
								value="dogs">Individual <input type="radio"
								ng-model="myVar" value="tuts">Group
						</p>
					</div>

					<div class="card-body">
						<div ng-switch="myVar">
							<div ng-switch-when="dogs">
								<form class="form-control-horizontal" ng-submit="assignTask()">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">Username</label>
										<div class="col-sm-6">
											<select ng-model="emp.username"
												class="form-control form-control-sm">
												<option ng-repeat="R in taskemployees"
													value="{{R.username}}">{{R.first_name}}
													{{R.middle_name}} {{R.last_name}}</option>
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">task_name</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="inputEmail3" placeholder="task_name" name="task_name"
												ng-model="emp.task_name">
										</div>
									</div>
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">task_description</label>
										<div class="col-sm-6">
											<textarea class="form-control form-control-sm"
												ng-model="emp.task_description" name="task_description"
												placeholder="task_description"></textarea>
										</div>
									</div>

									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">assigned_date</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												datepicker ng-model="emp.assigned_date"
												placeholder="assigned_date" />
										</div>
									</div>

									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">completion_date</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												datepicker ng-model="emp.completion_date"
												placeholder="completion_date" />
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-10 offset-sm-2">
											<button type="submit" class="btn btn-success btn-sm">Assign</button>
											<button type="reset" class="btn btn-danger btn-sm">Reset</button>
										</div>
									</div>
								</form>
							</div>
							<div ng-switch-when="tuts">
								<form class="form-control-horizontal" ng-submit="createGroup()">
									<div class="form-group row">
										<label for="inputEmail3" class="col-sm-2 col-form-label">Group Name</label>
										<div class="col-sm-6">
											<input type="text" class="form-control form-control-sm"
												id="inputEmail3" placeholder="group_name" name="group_name"
												ng-model="emp.group_name">
										</div>
									</div>
									<div class="form-group row">
										<div class="col-sm-10 offset-sm-2">
											<button type="submit" class="btn btn-success btn-sm">Create</button>
										</div>
									</div>	
								</form>
							</div>
						</div>


					</div>
				</div>
			</div>
		</div>

		<link
			href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/themes/smoothness/jquery-ui.min.css"
			rel="stylesheet" type="text/css" />
		<script
			src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
		<script
			src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
		<script src="/js/lib/angular.min.js"></script>
		<script src='<c:url value="/js/model/AdminModel.js"></c:url>'></script>
		<script src='<c:url value="/js/controller/AdminCtrl.js"></c:url>'></script>
		<script src='<c:url value="/js/service/AdminService.js"></c:url>'></script>
</body>
</html>