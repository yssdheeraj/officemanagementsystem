<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Employee Details</title>

<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

</head>

<body ng-app="EmployeeApp"
	ng-controller="EmployeeDetailsController">

	<div class="card text-white bg-warning">
		<h4 class="card-header">EmployeeProfile</h4>
		<div class="card-body">
			<form class="form-control-horizontal" ng-submit="submitEmployeeDetails()">

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">first_name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="first_name" name="first_name"
							ng-model="employee.first_name">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">middle_name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="middle_name" name="middle_name"
							ng-model="employee.middle_name">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">last_name</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="last_name" name="last_name"
							ng-model="employee.last_name">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">joined_date</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="joined_date" name="joined_date"
							ng-model="employee.joined_date">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">UserName</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="username" name="username"
							ng-model="employee.username">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">email</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="Email" name="email"
							ng-model="employee.email">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">password</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="password" name="password"
							ng-model="employee.password">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">gender</label>
					<div class="col-sm-6">
						<select ng-model="employee.gender"
							class="form-control form-control-sm">
							<option ng-repeat="G in genders" value="{{G.genderSelect}}">{{G.genderSelect}}</option>
						</select>

						<!-- <input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="gender" name="gender"
							ng-model="employee.gender"> -->
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">designation</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="designation" name="designation"
							ng-model="employee.designation">
					</div>
				</div>

				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">address</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="address" name="address"
							ng-model="employee.address">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">contact</label>
					<div class="col-sm-6">
						<input type="text" class="form-control form-control-sm"
							id="inputEmail3" placeholder="contact" name="contact"
							ng-model="employee.contact">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEmail3" class="col-sm-2 col-form-label">Upload
						Photo</label>
					<div class="col-sm-6">
						<!--   <input type="file" class="form-control form-control-sm" ng-file-select="onFileSelect($files)" ng-model="imageSrc"> -->
						<input type="file" class="form-control form-control-sm"
							ng-model="employee.profile_picture">

						<!-- <input type="file" class="form-control form-control-sm"
							id="inputEmail3" placeholder="address" name="profile_picture"
							file-model="employee.profile_picture"> -->
					</div>
				</div>
				<div class="form-group row">
					<div class="col-sm-10 offset-sm-2">
						<button type="submit" class="btn btn-info btn-sm">Submit</button>
						<button type="reset" class="btn btn-danger btn-sm">Reset</button>
					</div>
				</div>
			</form>
		</div>
	</div>


	<script src="/js/lib/angular.min.js"></script>
	<script src='<c:url value="/js/model/EmployeeModel.js"></c:url>'></script>
	<script
		src='<c:url value="/js/controller/EmployeeController.js"></c:url>'></script>
	<script
		src='<c:url value="/js/service/EmployeeService.js"></c:url>'></script>

</body>
</html>