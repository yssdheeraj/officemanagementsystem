<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sign in</title>

<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



</head>
<body ng-app="LoginApp" ng-controller="LoginController"
	ng-submit="userLogin()">

	<script src="/js/lib/angular.min.js"></script>
	<script src="/js/lib/angular-route.min.js"></script>

	<script src='<c:url value="/js/model/LoginModel.js"></c:url>'></script>
	<script src='<c:url value="/js/controller/LoginCtrl.js"></c:url>'></script>
	<script src='<c:url value="/js/service/LoginService.js"></c:url>'></script>
	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<div class="card text-white bg-info">
					<h4 class="card-header">Sign in</h4>
					<div class="card-body">
						<form class="form-control-horizontal">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-3 col-form-label">UserName</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										id="inputEmail3" placeholder="username" name="username"
										ng-model="user.username">
								</div>
							</div>
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-3 col-form-label">password</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										id="inputEmail3" placeholder="password" name="password"
										ng-model="user.password">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-sm-10 offset-sm-2">
									<button type="submit" class="btn btn-secondry btn-sm">Submit</button>
									<button type="reset" class="btn btn-danger btn-sm">Reset</button>
								</div>
							</div>
							<div class="form-group row">
								<a>Forget Password or Username ?</a>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>