<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>UploadFile</title>

<link href="/webjars/bootstrap/4.0.0/css/bootstrap.min.css"
	rel="stylesheet">
<script src="/webjars/popper.js/1.11.1/dist/umd/popper.min.js"></script>
<script src="/webjars/jquery/3.2.1/jquery.min.js"></script>
<script src="/webjars/bootstrap/4.0.0/js/bootstrap.min.js"></script>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>



</head>
<body ng-app="FileUploadApp" ng-controller="FileController">

	<script src="/js/lib/angular.min.js"></script>
	<%-- <scrip src='<c:url value="js/FileUpload.js"></c:url>'></script> --%>
	<script src="js/FileUpload.js"></script>

	<div class="container">
		<div class="row">
			<div class="col-6 offset-3">
				<div class="card text-white bg-info">
					<h4 class="card-header">UploadFileHere</h4>
					<div class="card-body">
						<form class="form-control-horizontal" ng-submit="submitFiles()">
							<div class="form-group row">
								<label for="inputEmail3" class="col-sm-3 col-form-label">UserName</label>
								<div class="col-sm-6">
									<input type="text" class="form-control form-control-sm"
										id="inputEmail3" placeholder="username" name="username"
										ng-model="demo.username">
								</div>
							</div>
							<input type="file" name="image"
								onchange="angular.element(this).scope().uploadedFile(this);"
								class="form-control">
							<button type="submit" class="btn btn-secondry btn-sm">Upload</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>