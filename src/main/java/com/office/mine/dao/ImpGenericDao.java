package com.office.mine.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

@Repository
@Transactional
@SuppressWarnings("unchecked")
public class ImpGenericDao<T> implements IGenericDao<T> {

	@PersistenceContext
	EntityManager entityManager;

	@Override
	public void save(T entity) {
		// TODO Auto-generated method stub
		entityManager.persist(entity);
	}

	@Override
	public T update(T entity) {
		// TODO Auto-generated method stub
		return entityManager.merge(entity);
	}

	@Override
	public void delete(T entity) {
		// TODO Auto-generated method stub
		entityManager.remove(entity);
	}

	@Override
	public List<T> findAll(T entity) {
		// TODO Auto-generated method stub
		return entityManager.createQuery("From " + entity.getClass().getName()).getResultList();
	}

	@Override
	public List<T> findAll(T entity, String condition) {
		// TODO Auto-generated method stub
		System.out.println("Final Query:########### " + "From " + entity.getClass().getName() + " " + condition);
		return entityManager.createQuery("From " + entity.getClass().getName() + " " + condition).getResultList();
	}

	@Override
	public T findOne(T entity, long id) {
		// TODO Auto-generated method stub
		return (T) entityManager.find(entity.getClass(), id);
	}

	@Override
	public T findOne(T entity, String condtion) {
		try {
			return (T) entityManager.createQuery("from " + entity.getClass().getName() + " " + condtion).getResultList()
					.get(0);
		} catch (IndexOutOfBoundsException e) {
			return null;
		}
	}

	@Override
	public boolean isExist(T entity, String condition) {
		return findOne(entity, condition) != null ? true : false;

	}
}
