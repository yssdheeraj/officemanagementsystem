package com.office.mine.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.office.mine.domain.UserDomain;
import com.office.mine.model.LoginDetails;
import com.office.mine.model.Users;
import com.office.mine.service.CommanService;
import com.office.mine.service.IGenericService;

@RestController
public class LoginController {

	private final static Logger logger = LoggerFactory.getLogger(LoginController.class);
	
	@Autowired
	private IGenericService<Users> iuserService;
	
	@Autowired
	private IGenericService<LoginDetails> ilogindetailsService;
	
	@Autowired
	private CommanService commanService;
	
	DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	Date date = new Date();
	String DATE_TIME = df.format(date).toString();
	
	@RequestMapping(value= {"/dologin"}, method=RequestMethod.POST)
	public ResponseEntity<?> doLogin(@RequestBody Users objUser){
	
		logger.info("\n\tRecieve Login Details: \n\t" + objUser);
		String query = "where username='"+objUser.getUsername()+"'and password='"+objUser.getPassword()+"'";
		Users user = iuserService.findOne(new Users(), query);
		
		logger.info("\n\tFind Login Details: \n\t" + user);
		
		logger.info("\n\t One Candidate Data: " + user);
		if (user==null) {
			logger.info("\n\t Sorry Not Found ! \n");
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			LoginDetails loginDetails = new LoginDetails();
			loginDetails.setUsername(objUser.getUsername());
			loginDetails.setPassword(objUser.getPassword());
			loginDetails.setLogindatetime(DATE_TIME);
			ilogindetailsService.save(loginDetails);
			return new ResponseEntity<>(user,HttpStatus.OK);
		}
		
	}
	
	
}
