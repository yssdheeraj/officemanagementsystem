package com.office.mine.controller;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.office.mine.domain.UserDomain;
import com.office.mine.model.Attendences;
import com.office.mine.model.Complaints;
import com.office.mine.model.Leaves;
import com.office.mine.model.Notice;
import com.office.mine.model.Role;
import com.office.mine.model.Tasks;
import com.office.mine.model.UserProfile;
import com.office.mine.model.Users;
import com.office.mine.service.FileStorageService;
import com.office.mine.service.IGenericService;
import com.office.mine.utils.TokenGenerator;
import com.office.mine.utils.UploadFileResponse;

@RestController
public class EmployeeController {

	private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

	@Value("${picture_profile.path}")
	private String picture_profile_path;
	
	@Autowired
	private IGenericService<Users> iuserService;

	@Autowired
	private IGenericService<UserProfile> iuserprofileService;

	@Autowired
	private IGenericService<Complaints> icomplaintsService;

	@Autowired
	private IGenericService<Leaves> ileaveService;
	
	@Autowired
	private IGenericService<Role> iroleService;
	
	@Autowired
	private IGenericService<Tasks> itaskService;
	
	@Autowired
	private IGenericService<Notice> inoticeService;
	
	@Autowired
	private IGenericService<Attendences> iattendenceService;

	
	@Autowired
	private FileStorageService fileStorageService;

	DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	Date date = new Date();
	String DATE_TIME = df.format(date).toString();

	
	@GetMapping(value= {"/rest/files/{filename:.+}"})
	public ResponseEntity<Resource> getFile(@PathVariable String filename) throws MalformedURLException{
		File file = new File(picture_profile_path + "/" + filename);
		logger.info("File Name: " + file);
		if (!file.exists()) {
			throw new RuntimeException("File Not Found");
		}
		Resource resource = new UrlResource(file.toURI());
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getName() + "\"")
                .body(resource); 
	}
	
	
	/*---------Fetch Roll----------*/
	@RequestMapping(value= {"roles"} , method=RequestMethod.GET)
	public ResponseEntity<List<Role>> getRole(){
		
		List<Role> roles = iroleService.findAll(new Role());
		if (roles.isEmpty()) {
			logger.info("No Role Avaliable");
		}
		logger.info("\n" + roles.size()+ " Role Find \n" + roles);
		return new ResponseEntity<List<Role>>(roles,HttpStatus.OK);
	}
	
	/*-------------------Create User-------------*/
	@RequestMapping(value = {
			"/postuser" }, method = RequestMethod.POST, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	public ResponseEntity<Void> PostUserDetails(@RequestParam(value = "first_name", required = false) String first_name,
			@RequestParam(value = "middle_name", required = false) String middle_name,
			@RequestParam(value = "last_name", required = false) String last_name,
			@RequestParam(value = "joined_date", required = false) String joined_date,
			@RequestParam(value = "username", required = true) String username,
			@RequestParam(value = "email", required = false) String email,
			@RequestParam(value = "password", required = false) String password,
			@RequestParam(value = "gender", required = false) String gender,
			@RequestParam(value = "user_role", required = false) String user_role,
			@RequestParam(value = "designation", required = false) String designation,
			@RequestParam(value = "address", required = false) String address,
			@RequestParam(value = "contact", required = false) String contact,
			@RequestPart(value = "profile_picture", required = true) MultipartFile profile_picture)  throws IOException{

		logger.info("\nfirst_name " + first_name + "\nmiddle_name " + middle_name + "\nlast_name " + last_name
				+ "\njoined_date " + joined_date + "\nusername " + username + "\nemail " + email + "\npassword "
				+ password + "\ngender " + gender + "\n user_role" + user_role +"\ndesignation " + designation + "\naddress " + address
				+ "\ncontact " + contact + "\nprofile_picture " + profile_picture.getOriginalFilename());

		String fileName = fileStorageService.storeFile(username, profile_picture);
		logger.info("Upadated File Name: " + fileName);

		Users user = new Users();
		user.setFirst_name(first_name);
		user.setMiddle_name(middle_name);
		user.setLast_name(last_name);
		user.setJoined_date(joined_date);
		user.setUsername(username);
		user.setEmail(email);
		user.setPassword(password);
		user.setDesignation(designation);
		user.setUser_role(user_role);
		logger.info("\nDate And Time" + DATE_TIME);
		user.setCreateAt(DATE_TIME);
		user.setUpdatedAt(DATE_TIME);
			
		TokenGenerator tokenGenerator = new TokenGenerator();
		String token = tokenGenerator.generateToken(username);
		logger.info("\nToken" + token);
		user.setRemember_token(token);

		UserProfile userProfile = new UserProfile();
		userProfile.setAddress(address);
		userProfile.setContact(contact);
		userProfile.setCreateAt(DATE_TIME);
		userProfile.setGender(gender);
		userProfile.setProfile_picture(fileName);
		userProfile.setUpdatedAt(DATE_TIME);
		user.setUserprofile(userProfile);

		
		
		logger.info("\n\tCreating Customer " + username);
		boolean isUserNameExists = iuserService.isExist(new Users(), "where username='" + username + "'");
		if (isUserNameExists) {
			logger.info("\n\t A User with User Name " + username + " is Already Exist\n");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		}
		
		iuserService.save(user);
		
		logger.info("\n\tSaved User Details\n" + user);
		return new ResponseEntity<Void>(HttpStatus.OK);

	}

	/*-------------------Update User by Id------------------*/
	@RequestMapping(value= {"user/updateuserprofile/{id}"} , consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	public ResponseEntity<?> updateUserDetail(@PathVariable("id") long id , @RequestBody UserDomain updatedata){
		
		UserProfile userProfile = iuserprofileService.findOne(new UserProfile(), id);
		logger.info("" + userProfile);
		
		if (userProfile == null) {
			logger.info(" \n No UserProfile found for id " +id);
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			userProfile.setAddress(updatedata.getAddress());
			userProfile.setContact(updatedata.getContact());
			userProfile.setGender(updatedata.getGender());
			userProfile.setUpdatedAt(DATE_TIME);
			iuserprofileService.update(userProfile);
			logger.info("\n\t UserProfile id " + id + " Update Sucessfully\n");
			return new ResponseEntity<>(HttpStatus.OK);
		}
	}
	
	/*-------------------Fetch Complaint By UserName------------------*/
	@RequestMapping(value= {"get/complaint/{username}"} , method=RequestMethod.GET)
	public ResponseEntity<List<Complaints>> fetchComplaint(@PathVariable("username") String username){
		
		String query = "where username='"+username+"'";
		List<Complaints> complaints = icomplaintsService.findAll(new Complaints(), query);
		if (complaints.isEmpty()) {
			logger.info(" \n No complaint found for username " +username);
			return new ResponseEntity<List<Complaints>>(HttpStatus.NOT_FOUND);
		}
		logger.info(" \n " +complaints.size()+" complaint found for username " +username+ " \nComplaint List " +complaints);
		return new ResponseEntity<List<Complaints>>(complaints,HttpStatus.OK);
	}
	
	
	/*-------------------Create Complaint------------------*/
	@RequestMapping(value = { "/postcomplaint" }, method = RequestMethod.POST)
	public ResponseEntity<Void> PostUserComplaint(@RequestBody Complaints complaint) {

		logger.info("Complaints data: " + complaint);
		complaint.setCreateAt(DATE_TIME);
		complaint.setUpdatedAt(DATE_TIME);

		logger.info("Complaint: " + complaint);
		icomplaintsService.save(complaint);
		logger.info("\n\tSaved Complaint\n");
		return new ResponseEntity<Void>(HttpStatus.OK);

	}

	/*------------------- Delete Complaint-------------------------*/
	@RequestMapping(value = { "/deleteComplaint/{id}" }, method = RequestMethod.DELETE)
	public ResponseEntity<Complaints> deleteComplaint(@PathVariable("id") long id) {

		Complaints complaint = icomplaintsService.findOne(new Complaints(), id);
		if (complaint == null) {
			logger.info("\n\tUnable to delete. complaint with id " + id + " not found\n");
			return new ResponseEntity<Complaints>(HttpStatus.NOT_FOUND);
		}
		icomplaintsService.delete(complaint);
		logger.info("\n\t Customer id " + id + " Delete Sucessfully\n");
		return new ResponseEntity<Complaints>(HttpStatus.OK);
	}

	/*------------------- Update Complaint-------------------*/
	@RequestMapping(value = {"/updateComplaint/{id}" }, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	public ResponseEntity<Complaints> updateComplaint(@PathVariable("id") long id, @RequestBody Complaints complaint) {

		Complaints currentComplaint = icomplaintsService.findOne(new Complaints(), id);

		if (currentComplaint != null) {
			currentComplaint.setComplaint(complaint.getComplaint());
			currentComplaint.setUpdatedAt(DATE_TIME);
			icomplaintsService.update(currentComplaint);
			logger.info("\n\t Complaint id " + id + " Update Sucessfully\n");
			return new ResponseEntity<Complaints>(HttpStatus.OK);
		} else {
			logger.info("\n\t Complaint with id " + id + " not found\n");
			return new ResponseEntity<Complaints>(HttpStatus.NOT_FOUND);
		}

	}

	/*-------------------Create Leave------------------*/
	@RequestMapping(value = { "/postleave" }, method = RequestMethod.POST)
	public ResponseEntity<Void> PostUserLeave(@RequestBody Leaves leave) {

		logger.info("Leave data: " + leave);
		leave.setCreateAt(DATE_TIME);
		leave.setUpdatedAt(DATE_TIME);

		logger.info("Complaint: " + leave);
		ileaveService.save(leave);
		logger.info("\n\tSaved Leave\n");
		return new ResponseEntity<Void>(HttpStatus.OK);

	}
	
	/*-------------------Fetch Leave By UserName------------------*/
	@RequestMapping(value= {"get/leave/{username}"} , method=RequestMethod.GET)
	public ResponseEntity<List<Leaves>> fetchLeave(@PathVariable("username") String username){
		
		String query = "where username='"+username+"'";
		List<Leaves> leaves = ileaveService.findAll(new Leaves(), query);
		if (leaves.isEmpty()) {
			logger.info(" \n No leave found for username " +username);
			return new ResponseEntity<List<Leaves>>(HttpStatus.NOT_FOUND);
		}
		logger.info(" \n " +leaves.size()+" leave found for username " +username+ " \nLeave List " +leaves);
		return new ResponseEntity<List<Leaves>>(leaves,HttpStatus.OK);
	}
	
	
	/*------------------- Delete Leave-------------------------*/
	@RequestMapping(value = { "/deleteLeave/{id}" }, method = RequestMethod.DELETE)
	public ResponseEntity<Leaves> deleteLeave(@PathVariable("id") long id) {

		Leaves leave = ileaveService.findOne(new Leaves(), id);
		if (leave == null) {
			logger.info("\n\tUnable to delete. leave with id " + id + " not found\n");
			return new ResponseEntity<Leaves>(HttpStatus.NOT_FOUND);
		}
		ileaveService.delete(leave);
		logger.info("\n\t Customer id " + id + " Delete Sucessfully\n");
		return new ResponseEntity<Leaves>(HttpStatus.OK);
	}
	
	/*------------------- Update Leave-------------------*/
	@RequestMapping(value = {"/updateLeave/{id}" }, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	public ResponseEntity<Leaves> updateLeave(@PathVariable("id") long id, @RequestBody Leaves updateleave) {

		Leaves leave = ileaveService.findOne(new Leaves(), id);

		if (leave != null) {
			leave.setStart_date(updateleave.getStart_date());
			leave.setEnd_date(updateleave.getEnd_date());
			leave.setReason(updateleave.getReason());
			leave.setUpdatedAt(DATE_TIME);
			ileaveService.update(leave);
			logger.info("\n\t Leave id " + id + " Update Sucessfully\n");
			return new ResponseEntity<Leaves>(HttpStatus.OK);
		} else {
			logger.info("\n\t Leave with id " + id + " not found\n");
			return new ResponseEntity<Leaves>(HttpStatus.NOT_FOUND);
		}

	}
	

	
	/*-------------------Retrieve All-----------------*/
	@RequestMapping(value = { "/fetchalluser" }, method = RequestMethod.GET)
	public ResponseEntity<List<Users>> fetchAll() {

		List<Users> employeedetails = iuserService.findAll(new Users());
		logger.info("\n\tEmployee Details\n" + employeedetails);
		if (employeedetails.isEmpty()) {
			return new ResponseEntity<List<Users>>(HttpStatus.NO_CONTENT);
		} else {
			return new ResponseEntity<List<Users>>(employeedetails, HttpStatus.OK);
		}

	}

	@PostMapping(value = { "/uploadFile" }, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
	/*
	 * public ResponseEntity<?> uploadFile(@RequestPart("file") MultipartFile file){
	 */
	public ResponseEntity<UploadFileResponse> uploadFile(@RequestParam("file") MultipartFile file,
			@RequestParam(value = "username") String username) {

		logger.info("Upload File: " + file);
		logger.info("Upload username: " + username);
		if (file.isEmpty()) {
			logger.info("\n\t Please Select a File");
		} else {
			// Get the file and it somewhere

			String fileName = file.getOriginalFilename();
			logger.info("\n\t File Name: " + fileName);
		}
		return new ResponseEntity<UploadFileResponse>(HttpStatus.OK);
	}

	
	// -------------------Retrieve Employee by username and
	// Password--------------------------------------------------------
	/*
	 * @RequestMapping(value= {"/fetch_employee"} , method=RequestMethod.POST)
	 * public ResponseEntity<EmployeeDetails> fetchEmployee(@RequestBody Employee
	 * objUserDomain){
	 * 
	 * String query =
	 * "where username='"+objUserDomain.getUsername()+"'and password='"
	 * +objUserDomain.getPassword()+"'";
	 * 
	 * EmployeeDetails employeeDetail = iemployeedetailsService.findOne(new
	 * EmployeeDetails(),query); logger.info("\n\tOne Employee Details\n" +
	 * employeeDetail); if (employeeDetail == null) { return new
	 * ResponseEntity<EmployeeDetails>(HttpStatus.NO_CONTENT); } else { return new
	 * ResponseEntity<EmployeeDetails>(employeeDetail,HttpStatus.OK); }
	 * 
	 * }
	 */
	
	/**************Get View Task By UserName*************/
	@RequestMapping(value= {"/get/view/task/{username}"} , method=RequestMethod.GET)
	public ResponseEntity<List<Tasks>> viewTask(@PathVariable("username") String username){
		
		String condition = "where username='"+username+"'";
		List<Tasks> tasks= itaskService.findAll(new Tasks(), condition);
		if (tasks.isEmpty()) {
			logger.info("No Task found for username "+username+"\n"+tasks );
			return new ResponseEntity<List<Tasks>>(HttpStatus.NOT_FOUND) ;
		} 
		logger.info("Total "+tasks.size()+" found for username "+username+"\n"+tasks );
		return new ResponseEntity<List<Tasks>>(tasks,HttpStatus.OK) ;
	}
	
	/**************Get View Attendence By UserName*************/
	@RequestMapping(value= {"/get/view/attendence/{username}"} , method=RequestMethod.GET)
	public ResponseEntity<List<Attendences>> viewAttendence(@PathVariable("username") String username){
		
		String condition = "where username='"+username+"'";
		List<Attendences> attendences= iattendenceService.findAll(new Attendences(), condition);
		if (attendences.isEmpty()) {
			logger.info("No Attendence found for username "+username+"\n"+attendences );
			return new ResponseEntity<List<Attendences>>(HttpStatus.NOT_FOUND) ;
		} 
		logger.info("Total "+attendences.size()+" found for username "+username+"\n"+attendences );
		return new ResponseEntity<List<Attendences>>(attendences,HttpStatus.OK) ;
	}

	/**************Get View Notice*************/
	@RequestMapping(value= {"/get/view/notice"} , method=RequestMethod.GET)
	public ResponseEntity<List<Notice>> viewNotice(){
		
		List<Notice> notices = inoticeService.findAll(new Notice());
		if (notices.isEmpty()) {
			logger.info("No Notice found");
			return new ResponseEntity<List<Notice>>(HttpStatus.NOT_FOUND) ;
		} 
		logger.info("Total "+notices.size()+" found \n"+notices );
		return new ResponseEntity<List<Notice>>(notices,HttpStatus.OK) ;
	}
	
	/***********Filter Attendence*********/
	@RequestMapping(value= {"/get/filter/attendence/{from}/{to}/{username}"})
	public ResponseEntity<?> filterAttendence(@PathVariable("from") String from , @PathVariable("to") String to,@PathVariable("username") String username){
		logger.info("Filter Attendence \n From: "+from+"\n To: "+to+" \n Username: "+username);
		
		String query = "where attendence_date BETWEEN '"+from+"' AND '"+to+"' AND username='"+username+"'";
		
		List<Attendences> attendenceList = iattendenceService.findAll(new Attendences(),query);
		
		
		logger.info("Found: "+attendenceList.size());
		logger.info("Found Attendence: "+attendenceList);
		return new ResponseEntity<>(attendenceList,HttpStatus.OK);
	}
}
