package com.office.mine.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.office.mine.domain.UserDomain;
import com.office.mine.model.Attendences;
import com.office.mine.model.Complaints;
import com.office.mine.model.Leaves;
import com.office.mine.model.Notice;
import com.office.mine.model.Tasks;
import com.office.mine.model.Users;
import com.office.mine.service.IGenericService;

@RestController
public class AdminController {

	private static final Logger logger = LoggerFactory.getLogger(AdminController.class);

	@Autowired
	private IGenericService<Users> iuserService;

	@Autowired
	private IGenericService<Attendences> iattendenceService;

	@Autowired
	private IGenericService<Tasks> itaskService;
	
	@Autowired
	private IGenericService<Notice> inoticeService;
	
	@Autowired
	private IGenericService<Complaints> icomplaintService;
	
	@Autowired
	private IGenericService<Leaves> ileaveService;

	
	DateFormat df = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	Date date = new Date();
	String DATE_TIME = df.format(date).toString();

	DateFormat onlydf = new SimpleDateFormat("dd-MM-yyyy");
	Date date1 = new Date();
	String DATE = onlydf.format(date1).toString();

	DateFormat onlytf = new SimpleDateFormat("HH:mm:ss");
	Date date2 = new Date();
	String TIME = onlytf.format(date2).toString();

	/*-------------------Create/Update Attendence------------------*/
	@RequestMapping(value = { "/postAttendence" }, method = RequestMethod.POST)
	public ResponseEntity<Void> PostEmployeeAttendence(@RequestBody Attendences attendence) {

		logger.info("Attendence data: " + attendence);

		/*
		 * String query = "Where username='" + attendence.getUsername() + "'";
		 * logger.info("Query: " + query); Attendences presentattendence =
		 * iattendenceService.findOne(new Attendences(), query);
		 * logger.info("Present Attendence Date: " +
		 * presentattendence.getAttendence_date());
		 * 
		 * if (presentattendence != null) { if
		 * (presentattendence.getAttendence_date().equals(DATE)) { logger.info("Equal");
		 * 
		 * attendence.setPresence(attendence.getPresence());
		 * attendence.setUpdatedAt(DATE_TIME); iattendenceService.update(attendence);
		 * logger.info("\n\tUpdate Attendence Successfully For UserName: " +
		 * attendence.getUsername()); } else { logger.info("Not Equal");
		 * attendence.setCreateAt(DATE_TIME); attendence.setUpdatedAt(DATE_TIME);
		 * attendence.setAttendence_date(DATE); attendence.setAttendence_time(TIME);
		 * 
		 * iattendenceService.save(attendence);
		 * logger.info("\n\tSaved Attendence Successfully For UserName: " +
		 * attendence.getUsername());
		 * 
		 * }
		 * 
		 * }
		 */
		logger.info("Attendence Not Aviliable in Table");
		attendence.setCreateAt(DATE_TIME);
		attendence.setUpdatedAt(DATE_TIME);
		attendence.setAttendence_date(DATE);
		attendence.setAttendence_time(TIME);

		iattendenceService.save(attendence);
		logger.info("\n\tSaved Attendence Successfully For UserName: " + attendence.getUsername());

		return new ResponseEntity<Void>(HttpStatus.OK);

	}

	@GetMapping(value = { "/get/today/attendence" })
	public ResponseEntity<List<Attendences>> todayAttendence() {

		String today = DATE;
		String query = "where attendence_date='" + today + "'";
		List<Attendences> todayAttendences = iattendenceService.findAll(new Attendences(), query);
		logger.info("\nToday " + todayAttendences.size() + " Attendences Found \n" + todayAttendences);
		return new ResponseEntity<List<Attendences>>(todayAttendences, HttpStatus.OK);
	}

	/*-------------------Get All Employee For Attendence------------------*/
	@RequestMapping(value = { "/get/all/employee" }, method = RequestMethod.GET)
	public ResponseEntity<List<Users>> getEmployeeAttendence() {

		List<Users> users = iuserService.findAll(new Users());
		logger.info("Total Users: " + users);
		List<Attendences> todayAttendences = iattendenceService.findAll(new Attendences(),
				"where attendence_date='" + DATE + "'");
		logger.info("Total TodayAttendences: " + todayAttendences);

		for (int i = 0; i < users.size(); i++) {
			boolean found = true;

			for (Attendences attendences : todayAttendences) {
				if (users.get(i).getUsername().equals(attendences.getUsername())) {
					logger.info("Match" + found);
					users.remove(i);
					i--;
					break;
				}
			}
		}

		logger.info("total Found users: " + users.size());
		logger.info("total Found users: " + users);
		return new ResponseEntity<List<Users>>(users, HttpStatus.OK);
	}

	/*------------------- Update Attendence-------------------*/
	@RequestMapping(value = {
			"/update/attendence/{username}" }, consumes = MediaType.APPLICATION_JSON_VALUE, method = RequestMethod.PUT)
	public ResponseEntity<Attendences> updateAttendence(@PathVariable("username") String username,
			@RequestBody Attendences attendence) {

		String today = DATE;
		Attendences objAttendence = iattendenceService.findOne(new Attendences(),
				"where username='" + username + "' and attendence_date='" + today + "'");
		logger.info("Today Attendence for username: " + username + "\n" + objAttendence);
		if (objAttendence != null) {
			objAttendence.setPresence(attendence.getPresence());
			objAttendence.setUpdatedAt(DATE_TIME);

			iattendenceService.update(objAttendence);
			logger.info("\n\t Attendences Update Sucessfully for username " + username + " \n");
			return new ResponseEntity<Attendences>(HttpStatus.OK);
		} else {
			logger.info("\n\t Attendence Not found\n");
			return new ResponseEntity<Attendences>(HttpStatus.NOT_FOUND);
		}

	}



	/*-------------------Create Task------------------*/
	@RequestMapping(value = { "/post/task" }, method = RequestMethod.POST)
	public ResponseEntity<Void> PostTask(@RequestBody Tasks task) {

		logger.info("Task data: " + task);
		String condition = "where username='"+task.getUsername()+"' and task_name='"+task.getTask_name()+"'";
		boolean istaskname = itaskService.isExist(new Tasks(), condition);
		if (istaskname) {
			logger.info("\n Task Name: "+task.getTask_name()+" is Already exist for username "+task.getUsername());
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		} else {
			task.setCreateAt(DATE_TIME);
			task.setUpdatedAt(DATE_TIME);
			task.setFlag(true);
			itaskService.save(task);
			logger.info("\nTask Saved Succesfully for username: "+task.getUsername());
			return new ResponseEntity<Void>(HttpStatus.OK);

		}
		
	}
	
	/*-------------------Create Notice------------------*/
	@RequestMapping(value = { "/post/notice" }, method = RequestMethod.POST)
	public ResponseEntity<Void> PostNotice(@RequestBody Notice notice) {

		logger.info("Notice data: " + notice);
		
		String condition = "where notice='"+notice.getNotice()+"'";
		boolean isnotice = inoticeService.isExist(new Notice(), condition);
		if (isnotice) {
			logger.info("\n Notice: "+notice.getNotice()+" is Already Exist");
			return new ResponseEntity<Void>(HttpStatus.CONFLICT);
		} else {
			notice.setCreateAt(DATE_TIME);
			notice.setUpdatedAt(DATE_TIME);
			inoticeService.save(notice);
			logger.info("\n Notice Saved Succesfully");
			return new ResponseEntity<Void>(HttpStatus.OK);

		}
		
	}
	
	/*-------------------Get All Employee------------------*/
	@RequestMapping(value = { "/get/employees" }, method = RequestMethod.GET)
	public ResponseEntity<List<Users>> getEmployeeTask() {
		
		String Role = "ROLE_EMPLOYEE";
		List<Users> users = iuserService.findAll(new Users(), "where user_role='"+Role+"'");
		logger.info("Total Users: " + users);
		logger.info("total: " + users.size());
		return new ResponseEntity<List<Users>>(users, HttpStatus.OK);
	}
	

	/**************Get All Report*************/
	@RequestMapping(value= {"/get/all/employee/report/{username}"} , method=RequestMethod.GET)
	public ResponseEntity<UserDomain> getAllReport(@PathVariable("username") String username){
		
		logger.info("Username: " +username);
		
		String condition = "where username='"+username+"'";
		
		List<Attendences> attendenceList = iattendenceService.findAll(new Attendences(), condition);
		logger.info("\n"+attendenceList.size()+" Found Attendence for Username: " +username+"\n"+attendenceList);
		
		List<Complaints> complaintList = icomplaintService.findAll(new Complaints(), condition);
		logger.info("\n"+complaintList.size()+" Found Complaint for Username: " +username+"\n"+complaintList);
		
		List<Leaves> leaveList = ileaveService.findAll(new Leaves(), condition);
		logger.info("\n"+leaveList.size()+" Found Leave for Username: " +username+"\n"+leaveList);
				
		List<Tasks> taskList = itaskService.findAll(new Tasks(), condition);
		logger.info("\n"+taskList.size()+" Found Task for Username: " +username+"\n"+taskList);
		
		Users user = iuserService.findOne(new Users(), condition);
		logger.info("\n UserDetails for Username: " +username+"\n"+user);
		
		UserDomain userdata = new UserDomain();
		userdata.setAttendenceList(attendenceList);
		userdata.setComplaintList(complaintList);
		userdata.setLeaveList(leaveList);
		userdata.setTaskList(taskList);
		userdata.setAddress(user.getUserprofile().getAddress());
		userdata.setContact(user.getUserprofile().getContact());
		userdata.setDesignation(user.getDesignation());
		userdata.setEmail(user.getEmail());
		userdata.setFirst_name(user.getFirst_name());
		userdata.setMiddle_name(user.getMiddle_name());
		userdata.setLast_name(user.getLast_name());
		userdata.setGender(user.getUserprofile().getGender());
		userdata.setJoined_date(user.getJoined_date());
		userdata.setUsername(user.getUsername());
		userdata.setAddress(user.getUserprofile().getAddress());
		
		logger.info("UserDomain Data: " +userdata);
		
		return new ResponseEntity<UserDomain>(userdata,HttpStatus.OK) ;
	}
	
}
