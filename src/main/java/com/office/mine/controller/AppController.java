package com.office.mine.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class AppController {

	@RequestMapping(value= {"/admin"})
	public ModelAndView adminDashboard() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/AdminDashboard");
		return model;
	}
	
	@RequestMapping(value= {"/mark_attendence"})
	public ModelAndView attendencePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/Attendences");
		return model;
	}
	
	@RequestMapping(value= {"/broadcast_notice"})
	public ModelAndView noticePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/Notice");
		return model;
	}

	@RequestMapping(value= {"/view_notice"})
	public ModelAndView viewNoticePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/ViewNotice");
		return model;
	}
	
	@RequestMapping(value= {"/view_task"})
	public ModelAndView viewTaskPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/ViewTask");
		return model;
	}
	
	@RequestMapping(value= {"/view_attendence"})
	public ModelAndView viewAttendencePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/ViewAttendence");
		return model;
	}

	
	@RequestMapping(value= {"/assign_task"})
	public ModelAndView taskPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/Tasks");
		return model;
	}
	
	
	
	@RequestMapping(value= {"/view_report"})
	public ModelAndView reportPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/Reports");
		return model;
	}
	
	@RequestMapping(value= {"/user"})
	public ModelAndView userPage() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/User");
		return model;
	}

	@RequestMapping(value= {"/user/profile"})
	public ModelAndView userDetailPage() {
		
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/UserProfile");
		return model;
	}
	
	
	@RequestMapping(value= {"/dashboard"})
	public ModelAndView dashboardPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("AdminDashboard");
		return model;
	}
	
	
	
	@RequestMapping(value= {"/employeedetail"})
	public ModelAndView usersPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/EmployeeDetails");
		return model;
	}
	
	@RequestMapping(value= {"/login"})
	public ModelAndView loginPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("Login");
		return model;
	}
	
	
	@RequestMapping(value= {"/ehome"})
	public ModelAndView employeehomePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/EmployeeDashboard");
		return model;
	}
	
	
	@RequestMapping(value= {"/ahome"})
	public ModelAndView adminhomePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/AdminDashboard");
		return model;
	}
	
	
	@RequestMapping(value= {"/leave"})
	public ModelAndView leavePage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/Leaves");
		return model;
	}
	
	
	@RequestMapping(value= {"/complaint"})
	public ModelAndView complaintPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("TEMPLATES/Complaint");
		return model;
	}
	
	@RequestMapping(value= {"/upload"})
	public ModelAndView uploadPage() {
		ModelAndView model = new ModelAndView();
		model.setViewName("UploadFile");
		return model;
	}
}
