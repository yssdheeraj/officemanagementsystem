package com.office.mine.service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public class FileStorageService {

	private final static Logger logger = LoggerFactory.getLogger(FileStorageService.class);
	
	@Value("${picture_profile.path}")
	private String picture_profile_path;
	
	public String storeFile(String username,MultipartFile  multipartFile) {
		 // Normalize file name
		//String fileName = StringUtils.cleanPath(multipartFile.getOriginalFilename());
		logger.info("UserName: " + username);
		//logger.info("Original FileName: " + fileName);
		//String USER_NAME = username;
		
		String FILE_NAME_PREFIX = "";
		String NEW_DOC_NAME = "";
		
		String NEW_FILE_NAME="";
		
		try {
			if (!multipartFile.isEmpty()) {
				File doc = new File(picture_profile_path + "/");
				logger.info("Picture Path: " +doc);
				if (!doc.exists()) {
					logger.info("Directory Not Exists: " +doc);
					doc.mkdirs();
				}
				
				String fileName="";
				String originalFileName  = multipartFile.getOriginalFilename();
				String words[] = originalFileName.split("\\ originalFileName+");
				for (int i = 0; i < words.length; i++) {
					fileName = words[i].replaceAll(" ","");
					logger.info("trim original fileName: " + fileName);
				}
				
				String destination = fileName.substring(0,fileName.indexOf(".jpg"));
				
				String newFile =  username;
				logger.info("\nOldFile: " +destination+ "\nNewFile: " +newFile);
				String replaceFile = destination.replace(destination, newFile);
				logger.info("Replace File Name: " +replaceFile);
				NEW_FILE_NAME = replaceFile+".jpg";
				logger.info("New File Name: " +NEW_FILE_NAME);
				
				String folderName = picture_profile_path + "/" + NEW_FILE_NAME;
				File file = new File(folderName);
				if (FILE_NAME_PREFIX != null) {
					NEW_DOC_NAME = FILE_NAME_PREFIX + NEW_FILE_NAME;
				} else {
					NEW_DOC_NAME = NEW_FILE_NAME;
				}
				
				String newPath = picture_profile_path + "/" + NEW_FILE_NAME;

				File newfile = new File(newPath);
				if (!file.isDirectory()) {
					file.renameTo(newfile);
				}
				
				byte [] bytes = multipartFile.getBytes();
				Path path = Paths.get(newPath);
				Files.write(path, bytes);
				
			
			} else {

			}
		} catch (Exception e) {
			logger.info("File Upload Exception: " +e);
			NEW_DOC_NAME = "";
		}
		
		
		
	
				
        return NEW_FILE_NAME;
	}
}
