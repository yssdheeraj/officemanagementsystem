package com.office.mine.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.office.mine.model.Complaints;

@Service
public class CommanService {

	private final static Logger logger = LoggerFactory.getLogger(CommanService.class);
	
	@Autowired
	private IGenericService<Complaints> icomplaintService;
	
	public List complaintbyId(String username) {
		
		List<Complaints> compaintList = null;
		compaintList = icomplaintService.findAll(new Complaints(), "where createdby='"+username+"'");
		logger.info("\n\tAvaliable Complaint for username: " + username + " is " + compaintList.size() + "\n\t" + compaintList);
		return compaintList;
	}
	
	
}
