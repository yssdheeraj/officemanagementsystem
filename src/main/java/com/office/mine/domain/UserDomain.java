package com.office.mine.domain;

import java.util.List;

import com.office.mine.model.Attendences;
import com.office.mine.model.Complaints;
import com.office.mine.model.Leaves;
import com.office.mine.model.Notice;
import com.office.mine.model.Tasks;
import com.office.mine.model.Users;

public class UserDomain {
	private String first_name;

	private String middle_name;

	private String last_name;

	private String joined_date;

	private String email;

	private String username;

	private String password;

	private String user_role;
	
	private String designation;

	private String gender;

	private String address;

	private String contact;

	private String profile_picture;

	private String createdby;

	private List<Complaints> complaintList;
	private List<Attendences> attendenceList;
	private List<Leaves> leaveList;
	private List<Tasks> taskList;

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getJoined_date() {
		return joined_date;
	}

	public void setJoined_date(String joined_date) {
		this.joined_date = joined_date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public String getProfile_picture() {
		return profile_picture;
	}

	public void setProfile_picture(String profile_picture) {
		this.profile_picture = profile_picture;
	}

	public String getCreatedby() {
		return createdby;
	}

	public void setCreatedby(String createdby) {
		this.createdby = createdby;
	}

	public List<Complaints> getComplaintList() {
		return complaintList;
	}

	public void setComplaintList(List<Complaints> complaintList) {
		this.complaintList = complaintList;
	}

	public List<Attendences> getAttendenceList() {
		return attendenceList;
	}

	public void setAttendenceList(List<Attendences> attendenceList) {
		this.attendenceList = attendenceList;
	}

	public List<Leaves> getLeaveList() {
		return leaveList;
	}

	public void setLeaveList(List<Leaves> leaveList) {
		this.leaveList = leaveList;
	}

	public List<Tasks> getTaskList() {
		return taskList;
	}

	public void setTaskList(List<Tasks> taskList) {
		this.taskList = taskList;
	}

	
	public String getUser_role() {
		return user_role;
	}

	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	@Override
	public String toString() {
		return "UserDomain [first_name=" + first_name + ", middle_name=" + middle_name + ", last_name=" + last_name
				+ ", joined_date=" + joined_date + ", email=" + email + ", username=" + username + ", password="
				+ password + ", user_role=" + user_role + ", designation=" + designation + ", gender=" + gender
				+ ", address=" + address + ", contact=" + contact + ", profile_picture=" + profile_picture
				+ ", createdby=" + createdby + ", complaintList=" + complaintList + ", attendenceList=" + attendenceList
				+ ", leaveList=" + leaveList + ", taskList=" + taskList + "]";
	}

}
