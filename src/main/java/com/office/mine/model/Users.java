package com.office.mine.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_users")
public class Users extends DateAudit{

	private static final long serialVersionUID = -4906547762452266731L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long Id;
	private String first_name;
	private String middle_name;
	private String last_name;
	private String joined_date;
	private String email;
	private String username;
	private String password;
	private String designation;
	private String remember_token;
	private String user_role;
	
	@OneToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userprofile_id")
	private UserProfile userprofile;

	@ManyToMany(fetch=FetchType.LAZY , cascade=CascadeType.ALL)
	@JoinTable(name="user_role" , joinColumns=@JoinColumn(name="user_id"),inverseJoinColumns=@JoinColumn(name="role_id"))
	private Set<Role> roles = new HashSet<Role>();
	
	/*@OneToMany(mappedBy="user")
	private Set<Complaints> complaints;*/
	
	public Long getId() {
		return Id;
	}

	public void setId(Long id) {
		Id = id;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getMiddle_name() {
		return middle_name;
	}

	public void setMiddle_name(String middle_name) {
		this.middle_name = middle_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getJoined_date() {
		return joined_date;
	}

	public void setJoined_date(String joined_date) {
		this.joined_date = joined_date;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDesignation() {
		return designation;
	}

	public void setDesignation(String designation) {
		this.designation = designation;
	}

	public String getRemember_token() {
		return remember_token;
	}

	public void setRemember_token(String remember_token) {
		this.remember_token = remember_token;
	}

	public UserProfile getUserprofile() {
		return userprofile;
	}

	public void setUserprofile(UserProfile userprofile) {
		this.userprofile = userprofile;
	}
	public String getUser_role() {
		return user_role;
	}
	public void setUser_role(String user_role) {
		this.user_role = user_role;
	}

	public Set<Role> getRoles() {
		return roles;
	}

	public void setRoles(Set<Role> roles) {
		this.roles = roles;
	}

	@Override
	public String toString() {
		return "Users [Id=" + Id + ", first_name=" + first_name + ", middle_name=" + middle_name + ", last_name="
				+ last_name + ", joined_date=" + joined_date + ", email=" + email + ", username=" + username
				+ ", password=" + password + ", designation=" + designation + ", remember_token=" + remember_token
				+ ", user_role=" + user_role + ", userprofile=" + userprofile + ", roles=" + roles + "]";
	}

	

	

}
