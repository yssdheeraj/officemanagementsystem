package com.office.mine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_tasks")
public class Tasks extends DateAudit{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5304444463758800810L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String task_name;
	private String task_description;
	private String assigned_date;
	private String completion_date;
	private String username;
	private boolean flag;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getTask_name() {
		return task_name;
	}
	public void setTask_name(String task_name) {
		this.task_name = task_name;
	}
	public String getTask_description() {
		return task_description;
	}
	public void setTask_description(String task_description) {
		this.task_description = task_description;
	}
	public String getCompletion_date() {
		return completion_date;
	}
	public void setCompletion_date(String completion_date) {
		this.completion_date = completion_date;
	}
	public boolean isFlag() {
		return flag;
	}
	public void setFlag(boolean flag) {
		this.flag = flag;
	}
	public String getAssigned_date() {
		return assigned_date;
	}
	public void setAssigned_date(String assigned_date) {
		this.assigned_date = assigned_date;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "Tasks [id=" + id + ", task_name=" + task_name + ", task_description=" + task_description
				+ ", assigned_date=" + assigned_date + ", completion_date=" + completion_date + ", username=" + username
				+ ", flag=" + flag + "]";
	}
	
	
}
