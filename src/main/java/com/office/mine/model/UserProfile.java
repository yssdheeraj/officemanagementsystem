package com.office.mine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_user_profile")
public class UserProfile extends DateAudit{

	private static final long serialVersionUID = -5788852133698399180L;
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;
		private String gender;
		private String address;
		private String contact;
		private String profile_picture;
		
		@OneToOne
		@PrimaryKeyJoinColumn
		private Users user;
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getGender() {
			return gender;
		}
		public void setGender(String gender) {
			this.gender = gender;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getContact() {
			return contact;
		}
		public void setContact(String contact) {
			this.contact = contact;
		}
		public String getProfile_picture() {
			return profile_picture;
		}
		public void setProfile_picture(String profile_picture) {
			this.profile_picture = profile_picture;
		}
		
		public Users getUser() {
			return user;
		}
		public void setUser(Users user) {
			this.user = user;
		}
		@Override
		public String toString() {
			return "UserProfile [id=" + id + ", gender=" + gender + ", address=" + address + ", contact=" + contact
					+ ", profile_picture=" + profile_picture + ", user=" + user + "]";
		}
		
		
}
