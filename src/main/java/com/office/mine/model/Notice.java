package com.office.mine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_notice")

public class Notice extends DateAudit{
	

	private static final long serialVersionUID = -5341928018518133711L;
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String notice;
	
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNotice() {
		return notice;
	}
	public void setNotice(String notice) {
		this.notice = notice;
	}
	@Override
	public String toString() {
		return "Notice [id=" + id + ", notice=" + notice + "]";
	}
	
	
	
}
