package com.office.mine.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import com.office.mine.utils.Role_Enum;

@Entity
@Table(name="tbl_roles")
public class Role {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	@Enumerated(EnumType.STRING)
	@NaturalId
	@Column(length=50 , nullable=false)
	private Role_Enum rolename;

	public Role() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Role(Long id, Role_Enum rolename) {
		super();
		this.id = id;
		this.rolename = rolename;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", rolename=" + rolename + "]";
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Role_Enum getRolename() {
		return rolename;
	}

	public void setRolename(Role_Enum rolename) {
		this.rolename = rolename;
	}

		
}
