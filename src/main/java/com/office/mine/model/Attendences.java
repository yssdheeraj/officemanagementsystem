package com.office.mine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_attendence")
public class Attendences extends DateAudit{

	private static final long serialVersionUID = -3989416870025005887L;
		@Id
		@GeneratedValue(strategy=GenerationType.AUTO)
		private Long id;
		private String attendence_date;
		private String attendence_time;
		private String presence;
		//private Boolean presence;
		private String username;
		
		
		public Long getId() {
			return id;
		}
		public void setId(Long id) {
			this.id = id;
		}
		public String getAttendence_date() {
			return attendence_date;
		}
		public void setAttendence_date(String attendence_date) {
			this.attendence_date = attendence_date;
		}
		
		
		public String getAttendence_time() {
			return attendence_time;
		}
		public void setAttendence_time(String attendence_time) {
			this.attendence_time = attendence_time;
		}
		
		public String getPresence() {
			return presence;
		}
		public void setPresence(String presence) {
			this.presence = presence;
		}
		public String getUsername() {
			return username;
		}
		public void setUsername(String username) {
			this.username = username;
		}
		@Override
		public String toString() {
			return "Attendences [id=" + id + ", attendence_date=" + attendence_date + ", attendence_time="
					+ attendence_time + ", presence=" + presence + ", username=" + username + "]";
		}
	
}
