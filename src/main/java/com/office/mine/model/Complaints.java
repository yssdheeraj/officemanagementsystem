package com.office.mine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_complaints")
public class Complaints extends DateAudit{
	private static final long serialVersionUID = 4538210520909951060L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String complaint;
	private String username;
	
/*	@ManyToOne(optional=false)
	@JoinColumn(name="user_id")
	private Users user;*/

	/*@ManyToOne
	@JoinColumn(name="user_id")
	private Users user;*/
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getComplaint() {
		return complaint;
	}

	public void setComplaint(String complaint) {
		this.complaint = complaint;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@Override
	public String toString() {
		return "Complaints [id=" + id + ", complaint=" + complaint + ", username=" + username + "]";
	}

	
}
