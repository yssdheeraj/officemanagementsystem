package com.office.mine.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.office.mine.utils.DateAudit;

@Entity
@Table(name="tbl_leaves")
public class Leaves extends DateAudit{
	private static final long serialVersionUID = 4715117010466937806L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	private String start_date;
	private String end_date;
	private String reason;
	private String username;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getStart_date() {
		return start_date;
	}
	public void setStart_date(String start_date) {
		this.start_date = start_date;
	}
	public String getEnd_date() {
		return end_date;
	}
	public void setEnd_date(String end_date) {
		this.end_date = end_date;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	@Override
	public String toString() {
		return "Leaves [id=" + id + ", start_date=" + start_date + ", end_date=" + end_date + ", reason=" + reason
				+ ", username=" + username + "]";
	}
	
}
