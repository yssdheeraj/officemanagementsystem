'use strict';

alert("Hello LoginController");

var app = angular.module('LoginApp.controllers' , []);

app.controller('LoginController' , ['$scope','$window','LoginService' , function($scope,$window,LoginService){
	
	$scope.user={};
	
	$scope.userLogin = function(){
		console.log($scope.user);
		//_doLogin($scope.user);
		LoginService._userLogin($scope.user).then(
				function successCallback(response){
						console.log(response);
					if (response.status==200) {
						
						/*localStorage.setItem("same_data" , JSON.stringify($scope.user));*/
						localStorage.setItem("same_data" , JSON.stringify(response.data));
						localStorage.setItem("env" , 'dev');
						
						var host = $window.location.host;
						
						/*var landingUrl = "/ehome"
						$window.location.href = landingUrl;*/
						
					if (response.data.user_role=='ROLE_EMPLOYEE') {
							
							var host = $window.location.host;
							
							var landingUrl = "/ehome"
							$window.location.href = landingUrl;
							
						} else if(response.data.user_role=='ROLE_ADMIN'){
							var host = $window.location.host;
							
							var landingUrl = "/ahome"
							$window.location.href = landingUrl;
						}
						else{
							var host = $window.location.host;
							
							var landingUrl = "/login"
							$window.location.href = landingUrl;

						}
					} else {
						
						var host = $window.location.host;
						
						var landingUrl = "/login"
						$window.location.href = landingUrl;
					}
				},
				function errorCallback(response){
					
					if (response.status==404) {
						
						var host = $window.location.host;
						
						var landingUrl = "/login"
						$window.location.href = landingUrl;
					} else {
						
						var host = $window.location.host;
						
						var landingUrl = "/login"
						$window.location.href = landingUrl;
					}
				}
				);
	}
	
	/*_doLogin = function(user){
		
	}*/
	
}]);