'use strict';

alert("Hello Admin Controller");

var app = angular.module('AdminApp.controlles', [])

app.directive("datepicker", function() {

	return {

		restrict : "A",

		link : function(scope, el, attr) {

			el.datepicker({

				dateFormat : 'dd-mm-yy'

			});

		}

	};

})

app.controller('AttendenceController', [
		'$scope',
		'$window',
		'$log',
		'AdminService',
		function($scope, $window, $log, AdminService) {

			$scope.employees = [];
			$scope.employee = {};
			$scope.todayAttendences = [];
			$scope.myDate = Date.now();

			$scope.PresentSelect = "Present";
			$scope.AbsentSelect = "Absent";

			$scope.employeeData = {};

			_fetchallemployeeforattendence();
			_fetchtodayattendence();

			function _fetchallemployeeforattendence() {
				AdminService._getallemployeeforattendence().then(
						function successCallback(response) {

							$scope.employees = response.data;
							console.log("Employees", $scope.employees)
						}, function errorCallback(response) {
							console.log(response)
						});
			}

			function _fetchtodayattendence() {
				AdminService._gettodayattendence().then(
						function successCallback(response) {

							$scope.todayAttendences = response.data;
							console.log("TodayAttendences",
									$scope.todayAttendences)
						}, function errorCallback(response) {
							console.log(response)
						});
			}

			$scope.save = function(E) {
				// debugger
				console.log(E);
				if (E.PresentSelect) {
					$log.info(E.PresentSelect);
					$scope.employeeData.presence = E.PresentSelect;
				}
				if (E.AbsentSelect) {
					$log.info(E.AbsentSelect);
					$scope.employeeData.presence = E.AbsentSelect;
				}

				$scope.employeeData.username = E.username;
				$log.info($scope.employeeData);

				AdminService._postAttendence($scope.employeeData).then(
						function successCallback(response) {
							console.log(response)
						}, function errorCallback(response) {
							console.log(response)
						});
			}

			$scope.updateAttendence = function(TA) {
				console.log(TA.presence)
				console.log(TA.username)
				AdminService._updateAttendence(TA).then(
						function successCallback(response) {
							console.log(response);
						}, function errorCallback(response) {
							console.log(response);
						});
			}

			$scope.GetValue = function(E) {
				// debugger;
				var message = "";
				console.log(E);
				var userName = E.username;
				console.log(E.Selected);
				var isSelected = E.Selected;
				var presentDate = $scope.myDate;
				message += "UserName: " + userName + "\n Presence: "
						+ isSelected + "\n";
				$log.info(message)
				$scope.employeeData.username = E.username;
				$scope.employeeData.presence = E.Selected;
				$log.info($scope.employeeData);
				// $window.alert(message);

				AdminService._postAttendence($scope.employeeData).then(
						function successCallback(response) {
							console.log(response)
						}, function errorCallback(response) {
							console.log(response)
						});
			}

		} ]);

app.controller('TaskController', [
		'$scope',
		'$log',
		'AdminService',
		function($scope, $log, AdminService) {

			$scope.taskemployees = [];
			$scope.data = {};

			_fetchallemployeefortask();

			function _fetchallemployeefortask() {
				AdminService._getallemployee().then(
						function successCallback(response) {

							$scope.taskemployees = response.data;
							console.log("taskemployees", $scope.taskemployees)
						}, function errorCallback(response) {
							console.log(response)
						});
			}

			$scope.assignTask = function() {
				console.log($scope.emp);
				AdminService._posttask($scope.emp).then(
						function successCallback(response) {
							console.log(response)
						}, function errorCallback(response) {
							console.log(response)
						});
			}

		} ]);

app.controller('NoticeController', [
		'$scope',
		'$log',
		'AdminService',
		function($scope, $log, AdminService) {

			$scope.postNotice = function() {
				$log.info($scope.admin);
				AdminService._postnotice($scope.admin).then(
						function successCallback(response) {
							$log.info(response);
						}, function errorCallback(response) {
							$log.info(response);
						});
			}

		} ]);

app.controller('ViewReportController', [
		'$scope',
		'$log',
		'AdminService',
		function($scope, $log, AdminService) {

			$scope.viewemployees = [];
			$scope.employeeReport=[];
			$scope.attendenceList=[];
			$scope.complaintList=[];
			$scope.leaveList=[];
			$scope.taskList=[];
			
			_fetchallemployeeforreport();

			function _fetchallemployeeforreport() {
				AdminService._getallemployee().then(
						function successCallback(response) {
							$scope.viewemployees = response.data;
							console.log("viewemployees", $scope.viewemployees)
							
						}, function errorCallback(response) {
							console.log(response)
						});
			}

			$scope.selectAction = function() {
				//debugger;
				console.log($scope.data);
				AdminService._getAllReport($scope.data.username).then(
						function successCallback(response) {
							
							$scope.employeeReport = response.data;
							$scope.attendenceList = $scope.employeeReport.attendenceList;
							$scope.complaintList = $scope.employeeReport.complaintList;
							$scope.leaveList = $scope.employeeReport.leaveList;
							$scope.taskList = $scope.employeeReport.taskList;
							
							console.log("employeeReport", $scope.employeeReport)
							console.log("attendenceList", $scope.attendenceList)
							console.log("complaintList", $scope.complaintList)
							console.log("leaveList", $scope.leaveList)
							console.log("taskList", $scope.taskList)
							
						}, function errorCallback(response) {
							console.log(response)
						});

			}

		} ]);