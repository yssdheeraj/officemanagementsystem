'use strict';

alert("Hello UserController");

var app = angular.module('UserApp.controllers', []);

app.directive("datepicker", function() {

	return {

		restrict : "A",

		link : function(scope, el, attr) {

			el.datepicker({

				dateFormat : 'dd-mm-yy'

			});

		}

	};

})

app.controller('UserController', [
		'$scope',
		'$log',
		'UserService',
		function($scope, $log, UserService) {

			$scope.genders = [ {
				genderSelect : "Male"
			}, {
				genderSelect : "Female"
			} ];

			$scope.user = {};
			$scope.roles = [];

			$scope.userdetails = [];

			_findRoles();

			function _findRoles() {
				UserService._roles().then(function successCallback(response) {
					$scope.roles = response.data;
					console.log($scope.roles);

				}, function errorCallback(response) {
					console.log(response);
				});
			}

			$scope.uploadedFile = function(element) {
				// debugger
				$scope.$apply(function($scope) {
					$scope.file = element.files[0];
					console.log("File: ", $scope.file);
				});
			}

			$scope.submitUser = function() {
				// debugger;
				$scope.user.profile_picture = $scope.file;
				$log.info("Create User:", $scope.user);
				console.log("Create User: ", $scope.user);

				UserService._saveUserData($scope.user).then(
						function successCallback(response) {
							console.log("Saved User Response : ", response);
						},
						function errorCallback(response) {
							console.log("Errorin user's data saved : ",
									response.statusText);
						});
			}

		} ]);

app.controller('HomeDasboardController', [
		'$scope',
		'UserService',
		function($scope, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}

			$scope.fullName = $scope.user.first_name + $scope.user.middle_name
					+ $scope.user.last_name;
			$scope.address = $scope.user.userprofile.address;
			$scope.contact = $scope.user.userprofile.contact;
			$scope.gender = $scope.user.userprofile.gender;

			console.log("Profile Picture: ", $scope.profile_picture);
			console.log($scope.fullName);
			console.log($scope.address);
			console.log($scope.contact);
			console.log($scope.gender);

			/*
			 * fetchAllEmployee();
			 * 
			 * function fetchAllEmployee(){
			 * EmployeeService._getemployeedata().then( function
			 * successCallback(response){ console.log(response); }, function
			 * errorCallback(response){ console.log(response); } ); }
			 */
		} ]);

app.controller('ComplaintController', [
		'$scope',
		'UserService',
		function($scope, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}

			$scope.employee = {
				id : "",
				complaint : "",
			};

			$scope.complaintsList = [];

			_refreshComplaintPageData();

			function _refreshComplaintPageData() {
				_clearForm();
				_getComplaintList();

			}

			console.log("UserName: ", $scope.user.username);
			function _getComplaintList() {
				$scope.username = $scope.user.username;
				UserService._getComplaintList($scope.username).then(
						function successCallback(response) {

							$scope.complaintsList = response.data;
							console.log("Complaint List : ",
									$scope.complaintsList);
						},
						function errorCallback(response) {
							console.log("Error in Complaint List Response : ",
									response);
						});

			}

			$scope.reset = function() {
				_refreshComplaintPageData();
			}

			$scope.postComplaint = function() {
				// debugger;
				$scope.employee.username = $scope.user.username;
				console.log($scope.employee)
				if ($scope.employee.id == "") {
					UserService._savedcomplaint($scope.employee).then(_success,
							_error);

				} else {
					UserService._updateComplaint($scope.employee).then(
							_success, _error);
				}
			}

			$scope.removeComplaint = function(c) {
				// debugger
				UserService._deleteComplaint(c).then(_success, _error);

			}

			$scope.editComplaint = function(c) {
				console.log('Edit Complaint For Update: ', c)
				$scope.employee.complaint = c.complaint
				$scope.employee.id = c.id;
			}

			function _success(response) {
				_refreshComplaintPageData();
				_clearForm();
			}

			function _error(response) {
				console.log(response);
				console.log(response.statusText);
			}

			// clear the from
			function _clearForm() {
				$scope.employee.id = "";
				$scope.employee.complaint = "";
			}

		} ]);

app.controller('LeaveController', [
		'$scope',
		'UserService',
		function($scope, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}

			$scope.employee = {
				id : "",
				start_date : "",
				end_date : "",
				reason : "",
			};

			$scope.leaveList = [];

			_refreshLeavepagedata();

			$scope.postLeave = function() {

				$scope.employee.username = $scope.user.username;
				console.log($scope.employee);
				if ($scope.employee.id == "") {
					UserService._savedleave($scope.employee).then(_success,
							_error)
				} else {
					UserService._updateleave($scope.employee).then(_success,
							_error)
				}

			}

			function _refreshLeavepagedata() {
				_clearForm();
				_getLeaveList();
			}

			$scope._reset = function() {
				_refreshLeavepagedata();
			}

			function _getLeaveList() {
				$scope.username = $scope.user.username;
				UserService._getLeaves($scope.username).then(
						function successCallback(response) {

							$scope.leaveList = response.data;
							console.log("leave List : ", $scope.leaveList);
						},
						function errorCallback(response) {
							console.log("Error in leave List Response : ",
									response);
						});

			}

			$scope.removeLeave = function(L) {
				// debugger
				UserService._deleteLeave(L).then(_success, _error);

			}

			$scope.editLeave = function(L) {
				console.log('Edit Leave For Update: ', l)
				$scope.employee.id = L.id;
				$scope.employee.start_date = L.start_date;
				$scope.employee.end_date = L.end_date;
				$scope.employee.reason = L.reason;

			}

			function _success(response) {
				_refreshLeavepagedata();
				_clearForm();
			}

			function _error(response) {
				console.log(response);
				console.log(response.statusText);
			}

			// clear the from
			function _clearForm() {
				$scope.employee.id = "";
				$scope.employee.start_date = "";
				$scope.employee.end_date = "";
				$scope.employee.reason = "";
			}

		} ]);

app.controller('UserProfileController', [
		'$scope',
		'UserService',
		function($scope, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}

			$scope.genders = [ {
				genderSelect : "Male"
			}, {
				genderSelect : "Female"
			} ];

			$scope.user.id = $scope.user.userprofile.id;
			$scope.user.gender = $scope.user.userprofile.gender;
			$scope.user.address = $scope.user.userprofile.address;
			$scope.user.contact = $scope.user.userprofile.contact;

			$scope.updateUserProfile = function() {
				// debugger
				console.log($scope.user);
				UserService._updateUserProfile($scope.user).then(
						function successCallback(response) {
							console.log(response);
						}, function errorCallback(response) {
							console.log(response);
						});
			}
		} ])

app.controller('ViewNoticeController', [
		'$scope',
		'$log',
		'UserService',
		function($scope, $log, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}

			$scope.notices = [];

			_fetchAllnotice();

			function _fetchAllnotice() {
				UserService._getnotice().then(
						function successCallback(response) {
							$log.info(response);
							$scope.notices = response.data;
						}, function errorCallback(response) {
							$log.info(response);
						});
			}

		} ])

app.controller('ViewTaskController', [
		'$scope',
		'$log',
		'UserService',
		function($scope, $log, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}

			$scope.tasks = [];

			_fetchAllTask();

			function _fetchAllTask() {
				$scope.username = $scope.user.username;
				UserService._gettaskbyusername($scope.username).then(
						function successCallback(response) {
							$log.info(response);
							$scope.tasks = response.data;
						}, function errorCallback(response) {
							$log.info(response);
						});
			}

		} ])

app.controller('ViewAttendenceController', [
		'$scope',
		'$log',
		'UserService',
		function($scope, $log, UserService) {

			if (localStorage.getItem('same_data') != null) {
				$scope.user = JSON.parse(localStorage.getItem('same_data'));
				$scope.env = localStorage.getItem("env");
				console.log("User Data: ", $scope.user);
				console.log("ENV", $scope.env);
			}
			
			$scope.emp={}
			$scope.filterAttendences=[];
			
			$scope.attendences = [];

			_fetchAllAttendence();

			function _fetchAllAttendence() {
				$scope.username = $scope.user.username;
				UserService._getattendencebyusername($scope.username).then(
						function successCallback(response) {
							$log.info(response);
							$scope.attendences = response.data;
						}, function errorCallback(response) {
							$log.info(response);
						});
			}
			
			$scope.findAttendence = function(){
				console.log($scope.emp)
				UserService._filterAttendence($scope.emp.from,$scope.emp.to,$scope.user.username).then(
						function successCallback(response) {
							$scope.filterAttendences = response.data;
							$log.info($scope.filterAttendences);
							
						}, function errorCallback(response) {
							$log.info(response);
						}
						);
			}

		} ])