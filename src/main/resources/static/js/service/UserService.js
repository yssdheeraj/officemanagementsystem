alert("Hello UserService");

var app = angular.module('UserApp.services',[]);

app.service('UserService',['$http' , function($http){
	
	var self= this;
	
	self._roles = function(){
		var url='roles';
		return $http.get(url);
	}
	
	self._saveUserData = function(user){
		// debugger
		console.log(user.address);
		var formData = new FormData();
		
		formData.append('first_name' , user.first_name);
		formData.append('middle_name' , user.middle_name);
		formData.append('last_name' , user.last_name);
		formData.append('joined_date' , user.joined_date);
		formData.append('username' , user.username);
		formData.append('email' , user.email);
		formData.append('password' , user.password);
		formData.append('gender' , user.gender);
		formData.append('user_role',user.user_role)
		formData.append('designation' , user.designation);
		formData.append('address' , user.address);
		formData.append('contact' , user.contact);
		formData.append('profile_picture' , user.profile_picture);
		
		
		var url='postuser';
	
		return $http.post(url,formData,{
			transformRequest : angular.identity,
			headers : {
				'Content-Type':undefined
			}
		});
	}
	

	self._savedcomplaint = function(employee){
		var url='postcomplaint';
		var config ={
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		
		var data = employee;
		return $http.post(url,data,config);
		
	}
	
	
	self._getComplaintList = function(username){
		var url='get/complaint/' +username;
		console.log('Get Complaint List Url: ' , url);
		return $http.get(url); 
	}
	
	
	
	self._updateComplaint = function(employee){
		var url='updateComplaint/'+employee.id;
		console.log('Update Complaint Url: ' , url)
		var config= {
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		return $http.put(url,employee,config)
	}
	
	self._deleteComplaint = function(c){
		var url='deleteComplaint/' + c.id;
		console.log("Delete Complaint URL: " , url);
		var data = c;
		return $http.delete(url,data);
	}
	
	// getEmployeeAllData
	self._getemployeedata = function(){
		var Url='fetch_all_employee';
		return $http.get(Url);
	}
	
	self._savedleave =function(employee){
		var url='postleave';
		var config ={
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		
		var data = employee;
		return $http.post(url,data,config);
		
	}
	
	self._getLeaves = function(username){
		var url='get/leave/' +username;
		console.log('Get leave List Url: ' , url);
		return $http.get(url); 
	}

	self._deleteLeave = function(L){
		var url='deleteLeave/' + L.id;
		console.log("Delete Leave URL: " , url);
		return $http.delete(url);
	}
	
	self._updateleave = function(employee){
		var url='updateLeave/'+employee.id;
		console.log('Update Leave Url: ' , url)
		var config= {
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		return $http.put(url,employee,config)
	}
	
	self._updateUserProfile = function(user){
		var url = 'updateuserprofile/'+user.id;
		console.log('Update UserDetail Url: ' , url)
		var config= {
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		return $http.put(url,user,config)
		
	}
	
	self._getnotice = function(){
		var Url='get/view/notice';
		return $http.get(Url);
	} 
	
	self._gettaskbyusername =function(username){
		var url='get/view/task/'+username;
		return $http.get(url); 
	}
	
	self._getattendencebyusername =function(username){
		var url='get/view/attendence/'+username;
		return $http.get(url); 
	}
	
	self._filterAttendence = function(from,to,username){
		var url='get/filter/attendence/'+from+'/'+to+'/'+username;
		console.log("Filter Attendencec Url: " ,url)
		return $http.get(url)
	}
	
}]);