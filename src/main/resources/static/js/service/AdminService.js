alert("Hello Admin Service");

var app = angular.module('AdminApp.serivces',[]);

app.service('AdminService',['$http' , function($http){
	
	var self = this;
	
	self._getallemployeeforattendence = function(){
		var url='/get/all/employee';
		return $http.get(url);
	}
	
	self._gettodayattendence = function(){
		var url="/get/today/attendence";
		return $http.get(url);
	}
	
	self._postAttendence = function(employeeData){
		var url = '/postAttendence';
		var config ={
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		var data = employeeData;
		return $http.post(url,data,config);
		
	}
	
	self._updateAttendence = function(TA){
		var url = '/update/attendence/'+TA.username;
		var config= {
				headers : {
					'Content-Type':'application/json;charset=utf-8;'
				}
		}
		return $http.put(url,TA,config)
	}
	
	
	self._getallemployee = function(){
		var url='/get/employees';
		return $http.get(url);
	}
	
	self._posttask = function(emp){
		var url = '/post/task';
		var config={
				headers:{'Content-Type':'application/json;charset=utf-8;'}
		}
		return $http.post(url,emp,config)
	}
	
	
	self._postnotice = function(admin){
		var url = '/post/notice';
		var config = {
				headers:{'Content-Type':'application/json;charset=utf-8;'}
		}
		
		return $http.post(url,admin,config)
	}
	
	self._getAllReport = function(username){
		var url='/get/all/employee/report/'+username;
		console.log("Report Url: " , url);
		return $http.get(url);
	}
}]);