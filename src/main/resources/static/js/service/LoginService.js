alert("Hello  LoginService");

var app = angular.module('LoginApp.services' , []);

app.service('LoginService' , ['$http', function($http){
	
	this._userLogin = function(user){
		var data = user;
		console.log("Data in LoginService: " ,  data);
		var url='dologin';
		var config ={
				headers : {
					'Content-Type' : 'application/json;charset=utf-8;'
				}
		}
		
		return $http.post(url,data,config);
	}
	
}]);