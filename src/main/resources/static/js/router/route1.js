var App = angular.module('AdminDashboardApp',['ngRoute']);

App.config(['$routeProvider', function($routeProvider) {
	$routeProvider
		.when('/attendence', {
			templateUrl: 'mytemplate/attendence',
			controller : "AttendenceController as aCtrl"
	/*		resolve: {
                async: ['ItemService', function(ItemService) {
                    return ItemService.fetchAllItems('computers');
               	}]
            }*/
		})
		.when('/tasks' , {
			templateUrl: 'mytemplate/tasks',
			controller : "TasksController as tCtrl"
		})
		.when('/view' , {
			templateUrl: 'mytemplate/view',
			controller : "ViewController as vCtrl"
		})
		.when('/users' , {
			templateUrl: 'mytemplate/users',
			controller : "UsersController as uCtrl"
		})
		.when('/complaints' , {
			templateUrl: 'mytemplate/complaints',
			controller : "ComplaintsController as cCtrl"
		})
		.otherwise({redirectTo:'/dashboard'});		
}]);



/*var app = angular.module('AdminDashboardApp', ['ngRoute']);
		
		app.config(function($routeProvider){
		    $routeProvider
		        .when('/attendence',{
		        	template : 'This is attendence page'
		            templateUrl: '/attendence',
		            controller: 'AttendenceController'
		        })
		        .when('/tasks',{
		        	template : 'This is tasks page'
		        
		        })
		        .when('/view',{
		        	template : 'This is view page'
		        })
		        .when('/users',{
		        	template : 'This is users page'
		        })
		        .when('/complaints',{
		        	template : 'This is complaints page'
		        })
		        .otherwise(
		            {
		            	redirectTo: '/',
		            	template : 'This is HomePage page'
		            }
		        );
		});*/