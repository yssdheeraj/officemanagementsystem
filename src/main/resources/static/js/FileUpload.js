alert("Hello file Upload")

var fileApp = angular.module('FileUploadApp', []);

fileApp.controller('FileController', ['$scope','FileService',function($scope, FileService) {

		$scope.demo={};
	
		
			$scope.uploadedFile = function(element) {
				debugger
				$scope.$apply(function($scope) {
					$scope.file = element.files[0];
					console.log("File: ", $scope.file);
				});
			}

			$scope.submitFiles = function() {
				debugger
				
				$scope.demo.file = $scope.file
				var file = $scope.demo.file
				var username = $scope.demo.username;
				FileService.uploadFileToUrl(file,username).then(
						function successCallback(response) {
							console.log("Successfully Upload : ", response);
						}, function errorCallback(response) {
							console.log("Errorin Upload : ", response);
						});
			}

		} ]);

fileApp.service('FileService', [ '$http', function($http) {

	this.uploadFileToUrl = function(file,username) {
		debugger
		var formData = new FormData();
		formData.append('file', file);
		formData.append('username' , username)
		
		var url = 'uploadFile';
		
		return $http.post(url, formData,{
            transformRequest : angular.identity,
            headers : {
                'Content-Type' : undefined
            }});
	}
} ]);
